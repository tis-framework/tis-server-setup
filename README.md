# tis-server-setup
```
    tis-server-setup
    Copyright (C) 2022  tis-framework

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
```

**NOTE: THIS IS NOT FINISHED YET, IT *WILL NOT* WORK PROPERLY**

This repository will host files that allow you to easily start your own live server using pure docker or create a development environment where you can live edit the source code that is used by your docker.