#    tis-server-setup
#    Copyright (C) 2022  tis-framework
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published
#    by the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>

# base image contains only the server artifact

# Create OS with all dependencies
FROM ubuntu:20.04 as base
# RUN apt update && apt upgrade -y
RUN apt-get update 
RUN apt-get install -y wget xz-utils openssh-server 
RUN apt-get update
RUN apt-get install -y zip unzip
RUN rm -rf /var/lib/apt/lists/

WORKDIR /fx-server
RUN mkdir /fx-server/server

# Install the server artifact
RUN wget -O fx.tar.xz https://runtime.fivem.net/artifacts/fivem/build_proot_linux/master/5181-9eba8dcc91ee4c6ab009fcf7a47837edf81efd1a/fx.tar.xz
# | tar xJ --strip-components=1
RUN tar -xf fx.tar.xz -C /fx-server/server
RUN rm -rf /fx-server/fx.tar.xz

EXPOSE 30120/udp 30120/tcp
EXPOSE 80

# prod image also contains the clone from the git repositories
FROM base as prod


# Install git
RUN apt-get update
RUN apt-get install -y git

# Clone all the modules you want into your server.

# server data
RUN git clone https://github.com/citizenfx/cfx-server-data.git server-data

# Copy over the .cfg files
COPY src/server.cfg /fx-server/server-data/server.cfg
COPY src/resources.cfg /fx-server/server-data/resources.cfg

WORKDIR /fx-server/server-data/resources/[local]
RUN mkdir ./[assets] ./[assets]/[cars] ./[assets]/[clothes] ./[assets]/[maps] ./[tis] ./[standalone]

ARG fork=tis-framework

# Standalone modules
WORKDIR /fx-server/server-data/resources/[local]/[standalone]
RUN git clone https://gitlab.com/${fork}/ghmattimysql.git
RUN git clone https://github.com/Bob74/bob74_ipl.git

# Connect Queue
RUN git clone https://github.com/Nick78111/ConnectQueue.git 
RUN mv ConnectQueue/connectqueue . && rm -rf ConnectQueue

# MenuV
RUN wget -O menuv.zip https://github.com/ThymonA/menuv/releases/download/v1.4.1/menuv_v1.4.1.zip
RUN unzip menuv.zip -d menuv && rm -rf menuv.zip

RUN git clone https://github.com/AvarianKnight/pma-voice.git

# TIS framework modules


# Actually start the server
WORKDIR /fx-server/server-data
CMD bash /fx-server/server/run.sh +exec server.cfg